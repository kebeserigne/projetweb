/* Module de recherche dans une base de vehicules */
const Sqlite = require('better-sqlite3');

let db = new Sqlite('db.sqlite');


/**
 * Cette fonction lit le contenu d'un véhicule à partir de son identifiant.
 * @param id
 * @returns {null|*}
 */
exports.read = (id) => {
    let vehicule = db.prepare('SELECT * FROM vehicule WHERE id = ?').get(id);
    if (vehicule !== undefined){
        return vehicule;
    }else {
        return null;
    }
};


/**
 * Cette fonction renvoie la liste des vehicules ordonnée par leurs identifiant
 * @returns {*|Promise<[any, any, any, any, any]>|Promise<[any, any, any, any, any, any, any, any, any, any]>|Promise<[any, any, any, any, any, any, any, any, any]>|Promise<[any, any, any, any, any, any, any, any]>|Promise<[any, any, any, any, any, any]>|Promise<[any, any, any, any, any, any, any]>|Promise<[any, any, any, any]>|Promise<[any, any, any]>|Promise<[any, any]>|Promise<any[]>|*[]}
 */
exports.getListOfVehicule = () => {
    let vehicules = db.prepare('SELECT * FROM vehicule ORDER BY id').all();
    if (vehicules !== undefined){
        return vehicules;
    }else return [];
};

/**
 * Cette fonction permet d'ajouter un véhicule dans le panier à partir de son identifiant.
 * @param id
 */
exports.add = (id) => {
   db.prepare('INSERT INTO basket (vehicule,type,model,couleur,img,price) VALUES (?,?,?,?,?,?)').run([id,this.read(id).type,this.read(id).model,this.read(id).couleur,this.read(id).img,this.read(id).price]);
}

/**
 * Cette fonction renvoie le panier .
 * @returns {null|*|Promise<[any, any, any, any, any]>|Promise<[any, any, any, any, any, any, any, any, any, any]>|Promise<[any, any, any, any, any, any, any, any, any]>|Promise<[any, any, any, any, any, any, any, any]>|Promise<[any, any, any, any, any, any]>|Promise<[any, any, any, any, any, any, any]>|Promise<[any, any, any, any]>|Promise<[any, any, any]>|Promise<[any, any]>|Promise<any[]>}
 */
exports.getBasket = () => {
    let basket = db.prepare('SELECT * FROM basket ORDER BY id').all();
    if (basket !== undefined){
        return basket;
    }else {
        return [];
    }
}
/**
 * Cette fonction créé un nouvel client dans la base et renvoie son identifiant.
 * @param name
 * @param email
 * @param password
 * @returns {*}
 */
exports.createClient = (name, email, password) => {
    let insert = db.prepare('INSERT INTO client (name, email, password) VALUES (?, ?, ?)');
    let id = insert.run([name, email, password]).lastInsertRowid;
    //console.log("newId = "+id);
    return id;
}

/**
 * Cette fonction renvoie l'id de l'utilisateur s'il est bien authentifié.
 * @param email
 * @param password
 */
exports.login = (email, password) => {
    let user = db.prepare('SELECT * FROM client WHERE email = ? AND password = ?').get([email, password]);
    if (user !== undefined){
        return user.id;
    }else return -1;
}

/**
 * Cette fonction supprime un véhicule contenu dans le panier à partir de son id.
 * @param id
 */
exports.remove = (id) => {
    db.prepare('DELETE FROM basket WHERE id = ?').run(id);
}

/**
 * Cette fonction renvoie nos agences.
 * @returns {*[]|*}
 */
exports.getAgencies = () => {
    let agencies = db.prepare('SELECT * FROM agence ORDER BY id').all();
    if (agencies !== undefined){
        return agencies;
    }else return [];
}

/**
 * Cette fonction permet d'obtenir un de nos agences à partir de son id.
 * @param id
 * @returns {null|*}
 */
exports.getAgence = (id) => {
    let agence = db.prepare('SELECT * FROM agence WHERE id = ?').get(id);
    if (agence !== undefined) return agence;
    else return null;
};

exports.getClient = () => {
    let client = db.prepare('SELECT * FROM client').all();
    if (client !== undefined) return client;
    else return [];
}