/* Server pour le de location de vehicules */
var express = require('express');
var mustache = require('mustache-express');

const cookieSession = require('cookie-session');
var Keygrip = require('keygrip');
var path = require('path');

var model = require('./model');
var app = express();


// parse form arguments in POST requests
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));

/* Ce module garde des informations de session dans un cookie crypté passé au client à chaque requête */
app.use(cookieSession({
   name: 'session',
   keys: new Keygrip(['key1', 'key2'], 'SHA384', 'base64')
}));

app.use(connected);

app.engine('html', mustache());
app.set('view engine', 'html');
app.set('views', './views');

/**** Routes pour voir les pages du site ****/

/* Retourne une page principale */
app.get('/', (req, res) => {
   res.render('index');
});

/* Retourne une page contenant la liste des véhicules */
app.get('/list-vehicule', (req, res) => {
   let data = {};
   data.vehicules = model.getListOfVehicule();
   res.render('list-vehicule',data);
});

/* Retourne le contenu d'un véhicule d'un identifiant "id"*/
app.get('/read/:id', (req, res) =>{
   let data = {};
   data.entry = model.read(req.params.id);
   res.render('read', data);
});

/* Retourne le contenu du panier */
app.get('/basket', (req, res) => {
   let data = {};
   data.basket = model.getBasket();
   res.render('basket', data);
});

/* Retourne le formulaire de création de compte pour un client*/
app.get('/new_client', (req, res) =>{
   res.render('new_client');
});

/* Retourne le formulaire d’authentification */
app.get('/login', (req, res) => {
   res.render('login');
});

/* Route qui désauthentifie l’utilisateur */
app.get('/logout', (req, res) => {
   req.session = null;
   res.redirect('/');
});

/*Route qui renvoie le formulaire de suppression d'un véhicule dans le panier*/
app.get('/delete/:id', (req, res) => {
   let data = {}
   data.entry = model.read(req.params.id);
   res.render('delete', data);
});

/* Route qui renvoie le contenu de la page agence */
app.get('/agence', (req, res) =>{
   let data = {};
   data.agence = model.getAgencies();
   res.render('agence', data);
});

/* Route qui permet de lire un de nos agences à partir de son "id" */
app.get('/read-agence/:id', (req, res) => {
   let data = {};
   data.agence = model.getAgence(req.params.id);
   res.render('read-agence', data);
});

/* Route qui renvoie le formulaire de paiement de la location */
app.get('/location', (req, res) => {
   res.render('location');
});

/**** Routes post ****/

/*Route pour ajouter un véhicule dans le panier à partir de son "id"*/
app.post('/read/:id', (req, res) => {
   let id = req.params.id;
   model.add(id);
   res.redirect('/read/' + id);
});

/* Route pour ajouter un client, et son id à la session et le redirige vers la page principale */
app.post('/new_client', (req, res) => {
   let id = model.createClient(req.body.name, req.body.email, req.body.password);
   if (id !== -1){
      req.session.user = id;
      res.redirect('/');
   }
});

/* Route qui vérifie si les identifiants passés à la requête sont valide, et ajoute son id à la session */
app.post('/login', (req, res) => {
   let id = model.login(req.body.email, req.body.password);
   if (id !== -1){
      req.session.user = id;
      res.redirect('/');
   }else res.redirect('/login');
});

/*Route qui supprime un véhicule dans le panier à partir de son "id" et redirige l'utilisateur vers le panier*/
app.post('/delete/:id', (req, res) =>{
   model.remove(req.params.id);
   res.redirect('/basket');
});



/**** middleware ****/

/* middleware pour gérer le contrôle d’accès */
function is_authenticated(req, res, next) {
   if (req.session.user !== undefined){
      return next();
   }else {
      res.status(401).send('unauthorized access');
   }
}

/* middleware appliqué à toutes les requêtes qui met la valeur authenticated à true si user est connecté */
function connected(req, res, next){
   if (req.session.user !== undefined){
      res.locals.authenticated = true;
      next();
   }else {
      res.locals.authenticated = false;
      next();
   }
}


app.listen(3000, () => console.log('listening on http://localhost:3000'));