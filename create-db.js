"use strict"

const fs = require('fs');
const Sqlite = require('better-sqlite3');

let db = new Sqlite('db.sqlite');

const vehicules = JSON.parse(fs.readFileSync("data.json"));
const agences = JSON.parse(fs.readFileSync("data-agency.json"));

/**** Suppression des tables si ça existent dans la base de donnée ****/
db.prepare('DROP TABLE IF EXISTS basket').run();
db.prepare('DROP TABLE IF EXISTS vehicule').run();
db.prepare('DROP TABLE IF EXISTS client').run();
db.prepare('DROP TABLE IF EXISTS agence').run();

/**** création de la table vehicule ****/
db.prepare('CREATE TABLE vehicule (id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT, model TEXT, couleur TEXT, img TEXT, price NUMBER)').run();

/**** création de la table client ****/
db.prepare('CREATE TABLE client (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT, password TEXT)').run();
/*exemple de client qui existe dans la base de données
* name : Papa, email : p@gmail.com, password : papa*/

/**** création de la table basket ****/
db.prepare('CREATE TABLE basket (id INTEGER PRIMARY KEY AUTOINCREMENT, vehicule NUMBER, type TEXT, model TEXT, couleur TEXT, img TEXT, price NUMBER,' +
    'CONSTRAINT fk_basket_client FOREIGN KEY (vehicule) REFERENCES vehicule (id))').run();

/**** création de la table agence ****/
db.prepare('CREATE TABLE agence (id INTEGER PRIMARY KEY AUTOINCREMENT, ville TEXT, lieu TEXT, telephone TEXT, email TEXT, img TEXT)').run();


/* insertion des données dans la table vehicule */
let insert = db.prepare('INSERT INTO vehicule VALUES (@id, @type, @model, @couleur, @img, @price)');

/* insertion des données dans la table agence */
let insert1 = db.prepare('INSERT INTO agence VALUES (@id, @ville, @lieu, @telephone, @email, @img)');

var transaction = db.transaction((vehicules) => {
   for (let id = 0; id < vehicules.length; id++){
       let vehicule = vehicules[id];
       vehicule.id = id;
       insert.run(vehicule);
   }
});
transaction(vehicules);

var transactionAgency = db.transaction((agences) => {
    for (let id = 0; id < agences.length; id++){
        let agence = agences[id];
        agence.id = id;
        insert1.run(agence);
    }
});
transactionAgency(agences);

